/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var domain = "https://greenpass.lafise.com/";
//var domain = "http://localhost:3000/";
var fromNotification = false;
var app = {
    setNotification: function(bool) {
        if (bool) {
            app.pushNotificationFCM();
        } else {
            window.plugins.PushbotsPlugin.unregister();
        }
    },
    loginSubmit: function(e){
        console.log(e);
        var code = (e.keyCode ? e.keyCode : e.which);
        if ( (code==13) || (code==10))
            {
              $('#log_user').submit();
            }
    },
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        this.optimizeSpeed();
        this.pageActions();
        animations.ajaxCall();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    //add the plugin 
    
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('backbutton', this.onBackEvent, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        if (window.cordova.logger) {
            window.cordova.logger.__onDeviceReady();
        }
        window.alert = function (txt) {
           navigator.notification.alert(txt, null, "GreenPass", "Aceptar");
        }
        //if (device.platform == "Android") {
        if(window.localStorage["alert_enable"] != undefined && window.localStorage["sessionNotification"] != undefined) 
        {
            if (window.localStorage["alert_enable"] == "enabled"){
                $(".alert_image").attr('src',"img/footer/alert.png");
                app.gpsRequest();
            } else {
               $(".alert_image").attr('src',"img/footer/alert_disabled.png"); 
            }
        } 
        //}
        //console.log("ESTA ES LA PLATAFORMA ", this.devicePlatform);
        navigator.splashscreen.hide();
        if(!window.isTablet){
            screen.lockOrientation('portrait');
        };
        app.receivedEvent('deviceready');
    },
    gpsRequest: function(){
        //cordova.plugins.locationAccuracy.canRequest(function(canRequest){
            //if(canRequest){
                cordova.plugins.locationAccuracy.request(function(){
                    console.log("Request successful");
                    if (app.notificationClicked) {
                        app.notificationClicked = false;
                    } else {
                        app.initializeGeofence();
                    }                  
                }, function (error){    
                    console.error("Request failed");
                    if(error){
                        // Android only
                        console.error("error code="+error.code+"; error message="+error.message);
                        if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
                            if(window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")){
                                cordova.plugins.diagnostic.switchToLocationSettings();
                            }
                        }
                    }
                }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY // iOS will ignore this
                );
            //}
        //});
    },
    initializeGeofence: function(){
        window.geofence.initialize().then(function () {
            console.log("Successful initialization");
            window.geofence.removeAll().then(function(){
                console.log("all data removed")
            }, function(reason){
                console.log("data not removed, the reason is: " + reason); 
            });
            app.addGeofence();
        }, function (error) {
            console.log("Error", error);
        });
    },
    //devicePlatform: device.platform,
    geofencingItems: new Array(),
    encodeUTF8: function(string){
        if (device.platform == "Android") {
            console.log("before browser " + string);
            console.log("conversion en browser" + encodeURIComponent(string));
            return encodeURIComponent(string);
        } else {
            return string;
        }
    },
    geofencingBuilder: function(promocion, index){
        var object = new Object();
        object.id = index+"-"+promocion.nombre_comercio+"-"+Math.floor((Math.random() * 30) + 1);
        object.latitude = +promocion.latitud;
        object.longitude = +promocion.longitud;
        object.radius = 100;
        object.transitionType = 1; // TransitionType.ENTER = 1, TransitionType.EXIT = 2, TransitionType.BOTH = 3
        var resourceId = Math.floor(Math.random() * 999)
        object.notification = {
            id:             resourceId,
            title:          promocion.nombre_comercio,
            text:           promocion.descripcion_promocion,
            openAppOnClick: true,
            smallIcon: 'res://icon',
            icon: 'file://img/geofence_icon.png',
            data: {
                id: resourceId,
                title: promocion.nombre_comercio,
                descripcion: promocion.descripcion_promocion,
            }
        };
        if (object.latitude != 0 && object.longitude != 0) {
            app.geofencingItems.push(object);
        }
    },
    geofencingSucursales: function(sucursales, comercio){
        sucursales.map( function(item, index){
                var object = new Object();
                object.nombre_comercio = item.sucursal.title;
                object.latitud = +item.sucursal.latitud;
                object.longitud = +item.sucursal.longitud;
                object.descripcion_promocion = comercio.descripcion_promocion;
                if (object.latitud != 0 && object.longitud != 0) {
                    app.geofencingBuilder(object, index);
                }
            }
        );
    },
    geofencingPromociones: function(promociones, index){
        var promocion = promociones.promocion;
        promocion.nombre_comercio = app.encodeUTF8(promocion.nombre_comercio).replace(/[`~!@#$^*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        promocion.descripcion_promocion = app.encodeUTF8(promocion.descripcion_promocion).replace(/[`~!@#$^*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        if (promocion.latitud !== 0 && promocion.longitud !== 0) {
            app.geofencingBuilder(promocion,index);
        }
        app.geofencingSucursales(promocion.sucursales, {nombre_comercio: promocion.nombre_comercio, descripcion_promocion: promocion.descripcion_promocion});
    },
    geofencingComercios: function(comercios){
        comercios.comercio.promociones.map(app.geofencingPromociones);
    },
    addGeofence: function(){ 
        $.ajax({
            url:domain+"/api/v1/geofencing.json",
            type:"GET",
            crossDomain: true,
            dataType:"json",   
            contentType:this.contentType,
            beforeSend: function() {
                $(".background-loader").addClass("block-background");
                $(".ui-loader").show();
            },
            complete: function() {  
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
            },
            success:function(data)
            {   
                var geofencing = data.comercios;
                geofencing.map(app.geofencingComercios);
                geoTarget = shuffle(app.geofencingItems)//.slice(0,20);
                window.geofence.addOrUpdate(geoTarget).then(function () {
                    console.log('Geofence successfully added');
                    $(".alert_image").attr('src',"img/footer/alert.png");
                    window.localStorage["alert_enable"] = "enabled"; 
                    app.watchGeofence();
                    app.transitionGeofence();
                }, function (reason) {
                    console.log('Adding geofence failed >> ' + reason);
                });
            },
            error:function(jqXHR,textStatus,errorThrown)
            {
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
                console.log(jqXHR.responseJSON.message); 
            }
        });            
    },
    watchGeofence: function(){
        window.geofence.getWatched().then(function(geofencesJson) {
            var geofences = JSON.parse(geofencesJson);
            console.log("get Watched");
        });
    },
    transitionGeofence: function(){
        window.geofence.onTransitionReceived = function(geofences) {
            console.log("transition received");
            geofences.forEach(function (geo) {
                console.log('Geofence transition detected', geo);
            });
        };
    },
    notificationClicked: false,
    onNotificationClicked: function(notificationData) {
        if (notificationData != undefined ) {
            app.notificationClicked = true;
            navigator.notification.alert(decodeURIComponent(notificationData.descripcion), null, decodeURIComponent(notificationData.title), "Aceptar");
            /*cookies.manageTokens();*/
            console.log('App opened from Geo Notification!' +  notificationData);
        }
    },
    pushNotificationFCM: function(){
        window.plugins.PushbotsPlugin.initialize("59de55699b823a48798b4576", {"android":{"sender_id":"86848700021"}});
        window.plugins.PushbotsPlugin.on("registered", function(token){
            console.log("Registration Id:" + token);
        });
        window.plugins.PushbotsPlugin.getRegistrationId(function(token){
            console.log("Registration Id:" + token);
        });
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        if (parentElement != null) {
            var listeningElement = parentElement.querySelector('.listening');
            var receivedElement = parentElement.querySelector('.received');

            listeningElement.setAttribute('style', 'display:none;');
            receivedElement.setAttribute('style', 'display:block;');
        }
    },
    closeApp: false,
    onConfirm: function(buttonIndex) {
        if (buttonIndex == 1){
            if (app.closeApp){
                navigator.app.exitApp();
            } else{
                $(".logout").click();
            }
        } else {
            app.closeApp = false;
        }
    },
    showConfirm: function(message) {
        navigator.notification.confirm(
            message,
            app.onConfirm,
            'GreenPass',
            ['Aceptar','Cancelar']
        );
    },
    onBackEvent: function(e){
        
        if($.mobile.activePage.is('#home')){
            app.closeApp = true;
            app.showConfirm('¿Desea salir de la aplicación?');
        } else {
            if($.mobile.activePage.is("#main_menu")){
                app.closeApp = false;
                app.showConfirm('¿Desea cerrar sesión?');
            }
            else {
                $.mobile.back();
            }
        }
    },
    back: function(){
        $.mobile.back();
    },
    alertEvent: function(){
        if(window.localStorage["alert_enable"] != undefined ) 
            {
                if(window.localStorage["alert_enable"] == "enabled" ){ 
                  $(".alert_image").attr('src',"img/footer/alert_disabled.png");
                  window.localStorage["alert_enable"] = "disabled";
                   window.geofence.removeAll().then(function(){
                        console.log("geofence data removed");
                    }, function(reason){
                        console.log("the reason is",reason); 
                    });
                } else { 
                    window.localStorage["alert_enable"] = "enabled"; 
                    app.gpsRequest();
               }                
            }
        else {
            $(".alert_image").attr('src',"img/footer/alert.png");
            window.localStorage["alert_enable"] = "enabled";
            app.gpsRequest();
        }
    },
    optimizeSpeed: function(){
            $.mobile.buttonMarkup.hoverDelay = 0;
            $.mobile.defaultPageTransition = 'none';
            $.mobile.defaultDialogTransition = 'none';
    },
    pageActions: function(){
        //delegate instead on deletate("page-id", "page-show", function())
        $( document ).on("pageshow", function() {
            //animations.storm();
            if(!$.mobile.activePage.is("#activate_account") && !$.mobile.activePage.is("#reset_password")) {
                cookies.manageTokens();
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
            }
            if($.mobile.activePage.is("#home")){
                if(window.localStorage["username_rem"] != undefined ) 
                {
                    if(window.localStorage["username_rem"] != "" ){                 
                      $('#user_email').val(window.localStorage["username_rem"]);    
                      $("#remember_me").attr('checked', true).checkboxradio("refresh");
                   }                
                }
                if (window.localStorage["userAvatar"] != undefined) {
                    var image = document.getElementById("indexGPAvatar"); 
                    image.onerror = function () {
                      this.src = 'img/big_user_icon.png'; // place your error.png image instead
                    };
                    image.src=window.localStorage["userAvatar"];
                    image.setAttribute("style", "-webkit-box-shadow: 0px 10px 20px 1px rgba(0,0,0,0.75);");
                    image.setAttribute("style", "-moz-box-shadow: 0px 10px 20px 1px rgba(0,0,0,0.75);");
                    image.setAttribute("style", "box-shadow: 0px 10px 20px 1px rgba(0,0,0,0.75);");
                }
            }
            authentication.init();  
            if($.mobile.activePage.is("#commerceList") 
                && contentBuilder.categorySelected != "" 
                && contentBuilder.categorySelected != undefined) {
                contentBuilder.loadPromotions(contentBuilder.categorySelected);
            }
            if($.mobile.activePage.is("#change_avatar")) {
                cameraGP.loadCameraPlugin();
            }
            if($.mobile.activePage.is("#myGreenPass")) {
                $.getScript( "js/barcode.js" )
                    .done(function( script, textStatus ) {
                        myGreenPass.loadGP();
                    })
                    .fail(function( jqxhr, settings, exception ) {
                    });
            }
        });
        $(document).on('pagebeforeshow', '.page', function (event, data) {
            $.mobile.silentScroll(0);
            var parameters = $(this).data("url").split("?")[1];
        });

        $( document ).on( "pageinit", ".page", function() {
            $( document ).on( "swipeleft swiperight", ".page", function( e ) {
                if ( $.mobile.activePage.jqmData( "panel" ) !== "open" ) {
                    if ( e.type === "swipeleft"  ) {
                        $( "#right-panel" ).panel( "open" );
                    } else if ( e.type === "swiperight" ) {
                        $( ".left-panel" ).panel( "open" );
                    }
                }
            });
            
            $('.bottom-panel').on("click", function(){
                $('.bottom-panel').animate({'bottom':'-100%'},300);
            });

            $('.settings').on('click', function (e) {
                $(this).css("z-index", "auto"); /* FF fix */
                $(".ui-sub-panel-open")
                    .addClass('ui-sub-panel-close ui-sub-panel-animate')
                    .removeClass("ui-sub-panel-open");
                $('.submenu')
                    .addClass('ui-sub-panel-open ui-sub-panel-animate')
                    .removeClass("ui-sub-panel-close");
            });

            /* open submenu2 */
           
            /* close panel where close button is clicked */
            $('.panel-close').on('click', function () {
                $(this)
                    .closest(".ui-sub-panel")
                    .addClass('ui-sub-panel-close ui-sub-panel-animate')
                    .removeClass("ui-sub-panel-open");
            });
            /* return submenu back to original position */
            $(".submenu").on("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", function () {
                var position = $(this).offset().left;
                if (position < 0) {
                    $(this).removeClass("ui-sub-panel-close ui-sub-panel-animate");
                }
            });


        });
    }
};


var myGreenPass = {
    loadAvatar: function(){
        $.ajax({
            url:domain+"/api/v1/tokens",
            data:{token: $('meta[name="csrf-token"]').attr('content')},
            type:"GET",
            crossDomain: true,
            dataType:"json",   
            contentType:this.contentType,
            beforeSend: function() {
                $(".background-loader").addClass("block-background");
                $(".ui-loader").show();
            },
            complete: function() {  
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
            },
            success:function(data)
            {   
                if (data.image_source != null) {
                    window.localStorage["userAvatar"] = domain+data.image_source; 
                }
            },
            error:function(jqXHR,textStatus,errorThrown)
            {
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
                console.log(jqXHR.responseJSON.message)  
            }
        });
    },
    loadGP: function(){
        $.ajax({
            url:domain+"/api/v1/tokens",
            data:{token: $('meta[name="csrf-token"]').attr('content')},
            type:"GET",
            crossDomain: true,
            dataType:"json",   
            contentType:this.contentType,
            beforeSend: function() {
                $(".background-loader").addClass("block-background");
                $(".ui-loader").show();
            },
            complete: function() {  
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
            },
            success:function(data)
            {   
                var image = document.getElementById("miGPImage");
                if (data.image_source != null) {
                    image.src=domain+data.image_source;
                    image.setAttribute("style", "-webkit-box-shadow: 0px 10px 20px 1px rgba(0,0,0,0.75);");
                    image.setAttribute("style", "-moz-box-shadow: 0px 10px 20px 1px rgba(0,0,0,0.75);");
                    image.setAttribute("style", "box-shadow: 0px 10px 20px 1px rgba(0,0,0,0.75);");
                }
                document.getElementById("employee_name").innerHTML = data.name;
                document.getElementById("employee_number").innerHTML = data.carnet;
                document.getElementById("employee_id").innerHTML = data.cedula;
                document.getElementById("employee_date").innerHTML = data.employee_start_date;
                document.getElementById("employee_company").innerHTML = data.company_name;
                generateBarCode(data.carnet+"BL");
            },
            error:function(jqXHR,textStatus,errorThrown)
            {
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
                console.log(jqXHR.responseJSON.message)  
            }
        });
    }
}
var retries = 0;
var pictureSource;   // picture source
var destinationType; // sets the format of returned value
var filePath = null;
var cameraGP = {
    loadCameraPlugin: function(){
        console.log('Received Event');
        destinationType = navigator.camera.DestinationType.FILE_URI;
        pictureSource = navigator.camera.PictureSourceType.PHOTOLIBRARY; 
    },
    clearCache: function(){
        navigator.camera.cleanup();
    },
    onCapturePhoto: function(fileURI) {
        
        plugins.crop(function success (newPath) {
            cameraGP.displayImageByFileURL(newPath);
            $(".ui-loader").hide();
            $(".background-loader").removeClass("block-background");
        }, function fail () {
            $(".ui-loader").hide();
            $(".background-loader").removeClass("block-background");
            alert("Ha ocurrido un error inesperado, intentelo nuevamente");
        }, fileURI, {quality:100,targetWidth: 500,targetHeight: 500,})    

    },
    getPhoto: function() {
        // Retrieve image file location from specified source
        navigator.camera.getPicture(cameraGP.onCapturePhoto, cameraGP.onFail, {
            quality: 100,
            destinationType: destinationType.FILE_URI,
            sourceType: pictureSource
        });
    },
    capturePhoto: function() {
        $(".background-loader").addClass("block-background");
        $(".ui-loader").show();
        navigator.camera.getPicture(cameraGP.onCapturePhoto, cameraGP.onFail, {
            quality: 100,
            targetWidth: 500,
            targetHeight: 500,
            destinationType: destinationType.FILE_URI,
            correctOrientation: true
        });
    },
    onFail: function(message){
        alert('La operación no fue completada: ' + message);
        $(".ui-loader").hide();
        $(".background-loader").removeClass("block-background");
    },
    displayImageByFileURL: function(fileURI) {
        var elem =  document.getElementById("avatarImage");
        elem.src = fileURI;
        filePath = fileURI;
    },
    upload: function(){
        if (filePath == null) {
            $(".ui-loader").hide();
            $(".background-loader").removeClass("block-background");
        } else {
            $(".background-loader").addClass("block-background");
            $(".ui-loader").show();
            var win = function(r) {
                cameraGP.clearCache();
                retries = 0;
                alert('El archivo ha sido cargado exitosamente!!');
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
            };
            var fail = function (error) {
                /*if (retries == 0) {
                    retries ++
                    setTimeout(function() {
                        cameraGP.onCapturePhoto(filePath)
                    }, 1000)
                } else {
                    retries = 0;*/
                    cameraGP.clearCache();
                    alert(error);
                //}
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
            }; 

            var croppedFileName = filePath.substr(filePath.lastIndexOf('/') + 1);
            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.httpMethod = "PUT"
            options.fileName = croppedFileName.substr(0,croppedFileName.lastIndexOf('?'));
            options.mimeType = "image/jpeg";
            options.params = {}; // if we need to send parameters to the server request
            var ft = new FileTransfer();
            ft.upload(filePath, encodeURI(domain +"/api/v1/change_avatar/"+$('meta[name="csrf-token"]').attr('content')), win, fail, options);
        }
    },

};


var animations  = { 
    animInterval: function() {
        $("#main_menu .tile_card.effect__random").each(function(){
            var item = $(this);
            setInterval(function() {                
                if(item.hasClass("eje-x")) {
                        item.addClass("flippedX");
                } else {
                        item.addClass("flipped");
                }                
                setInterval(function() {                
                    if (item.hasClass("flippedX") || item.hasClass("flipped")) {
                        item.removeClass("flippedX");
                        item.removeClass("flipped");
                    }               
                },3000);
            },(10+ Math.floor(Math.random() * 30))*1000);         
        });       
    },
    storm: function(){
        $("#menu .box").each(function(){
            d = Math.random()*1000;
            var random = 1 + Math.floor(Math.random() * 10);
                if (random%2 == 0) {
                        $(this).addClass("rotateX");
                }else if (random%3 == 0){
                    $(this).addClass("rotateY");
                }else {
                    $(this).addClass("appear");   
                } 
        }); 
    },
    ajaxCall: function(){
        $(document).on({
            ajaxStart: function() { $('body').addClass("loading");},
            ajaxStop: function() { $('body').removeClass("loading");}
        });
    }
}

var cookies  = {
    // Application Constructor
    manageTokens: function(){
        var sessionNotification = window.localStorage["sessionNotification"];
        if (sessionNotification != undefined) {
            this.setCookie("access_token",sessionNotification);
        }

        var sessionToken = this.getCookie('access_token');
        var sessionStorage = this.getSessionStorage('access_token');// Get item    

        if (sessionStorage != undefined) {
            $('meta[name="csrf-token"]').attr('content', sessionStorage);
            if($.mobile.activePage.is("#home")){
                if (window.localStorage["pending_pass"] != undefined && window.localStorage["pending_pass"] == true) {
                    window.location.replace("activation_change_password.html");
                } else {
                    window.location.replace("main_menu.html");
                }
            }
        } else {
            if (sessionToken != undefined) {
                $('meta[name="csrf-token"]').attr('content', sessionToken);
                if($.mobile.activePage.is("#home")){
                    window.location.replace("main_menu.html");
                }
            } else {
                $.mobile.changePage("index.html", { transition: "slidedown", changeHash: false });
                //window.location.replace("index.html");
            }   
        }
        
        $.ajaxSetup({
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        });
    },
    setCookie: function(name, value, expires) {
        //if(!expires)
        //  expires = 1
        //var expireDate = new Date() //coge la fecha actual
        //crea la cookie: incluye el nombre, la caducidad y la ruta donde esta guardada
        //cada valor esta separado por ; y un espacio
        document.cookie = name + "=" + escape(value);// + "; expires=" + expireDate.toGMTString();
        this.setSessionStorage(name,value);
    },
    deleteCookie: function(name) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        this.deleteSessionStorage(name);
    },
    getCookie: function(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }, 
    getSessionStorage: function(name){
        if(typeof(Storage) !== "undefined") {
            return sessionStorage.getItem(name);// Get item
        }
    },
    setSessionStorage: function(name, value){
        if(typeof(Storage) !== "undefined") {
        sessionStorage.setItem(name,value);
        window.localStorage["sessionNotification"] = value;
        } else {
            // Sorry! No Web Storage support..
        }
    },
    deleteSessionStorage: function(name, value){
        if(typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
            //localStorage.setItem("lastname", "Smith");
            sessionStorage.removeItem("access_token");
            window.localStorage.removeItem("sessionNotification"); 
        } else {
            // Sorry! No Web Storage support..
        }
    }
};

var sessionState = {
    logout: function() {
        app.setNotification(false);
        window.location.replace("index.html");
        $('meta[name="csrf-token"]').attr('content', "");
        //deleteSessionStorage("access_token");
        cookies.deleteCookie("access_token");
        //navigator.vibrate(2000);
    }
}

Array.prototype.chunk = function ( n ) {
    if ( !this.length ) {
        return [];
    }
    return [ this.slice( 0, n ) ].concat( this.slice(n).chunk(n) );
};
var animation = ['eje-y', 'eje-x'];
var itemFlag = true; //used to change the grid view order on layout.
var contentBuilder={
    buildCategoryElement: function(backgroundImage, category, ancestorTile, noParent){       
        var card_front = document.createElement("div");
        var card_back = document.createElement("div");
        var legend= document.createElement("div");
        var parentTile = document.createElement("div");
        var childTile = document.createElement("div");

        var logoSRC= domain + category.image.image.url
        var frontLogo = document.createElement("img");
        frontLogo.src = logoSRC;
        var backLogo = document.createElement("img");
        backLogo.src = logoSRC;
        card_front.appendChild(frontLogo);
        card_back.appendChild(backLogo);
        parentTile.className = "tile is-parent tile_card effect__random " + animation[Math.floor(Math.random() * animation.length)];
        childTile.className = "tile is-child box"
        card_front.className = "card__front";
        card_back.className = "card__back";
        legend.className = "legend";  
        
        childTile.appendChild(backgroundImage);
        childTile.dataset.id = category.id;
        childTile.appendChild(card_front);
        childTile.appendChild(card_back);
        legend.innerHTML = category.title; 
        childTile.appendChild(card_back);
        childTile.appendChild(legend);             
        parentTile.appendChild(childTile);
        if (noParent){
            return childTile;
        }else {
            parentTile.appendChild(childTile);
        }
        if (ancestorTile!=null) {
            ancestorTile.className = "tile is-ancestor";
            ancestorTile.appendChild(parentTile);
            return ancestorTile;
        } else {
            return parentTile
        }
    },
    buildPromotionElement: function(backgroundImage, promotion, ancestorTile, tileClass, noParent){
        var ticket = document.createElement("div");
        var center = document.createElement("div");
        var legend= document.createElement("div");
        var discount= document.createElement("a");
        var parentTile = document.createElement("div");
        var childTile = document.createElement("div");
        var arrow = document.createElement("div");

        var imgPromotionSRC= domain + promotion.promocion.imagen;

        discount.innerHTML = promotion.promocion.descuento + "%";
        legend.innerHTML = promotion.comercio.nombre;

        parentTile.className = "tile is-parent tile_card" ;
        childTile.className = "tile is-child box";
        childTile.dataset.id = promotionDataArray.lastIndexOf(promotion);
        childTile.dataset.idPromotion = promotion.promocion.id;
        center.className = "center";
        ticket.className = "ticket "+tileClass;
        legend.className = "legend";
        arrow.className = "triangle-down";  

        center.appendChild(discount);
        ticket.appendChild(center);
        childTile.appendChild(backgroundImage);
        ticket.appendChild(arrow);
        childTile.appendChild(ticket);
        childTile.appendChild(legend);
        childTile.setAttribute("style", "background: url('"+imgPromotionSRC+"') no-repeat scroll center center; background-size: cover");
        if (noParent){
            return childTile;
        }else {
            parentTile.appendChild(childTile);
        }
        if (ancestorTile!=null) {
            ancestorTile.className = "tile is-ancestor";
            ancestorTile.appendChild(parentTile);
            return ancestorTile;
        } else {
            return parentTile
        }
    },
    buildGrid: function(data, isCategory){
        var elementsData = shuffle(data).chunk(4);
        var first = true;  

        [].forEach.call(elementsData, function(elementsGroup) {

        //for (var iterator = 0, len = elementsData.length ; iterator < len; iterator++) {    
 //       for (var iterator in categoriesData) {         
            var imgBG_big = document.createElement("img");
            var imgBG_small = document.createElement("img");
            var imgBG_med_v = document.createElement("img");
            var imgBG_med_h = document.createElement("img");

            var mainContainer = document.getElementsByClassName((isCategory) ? "mobile-category" : "mobile-promotion").item(0);
            imgBG_big.src = "img/back_main.png";
            imgBG_small.src = "img/square_main.png";
            imgBG_med_v.src = "img/midle_main_v.png";
            imgBG_med_h.src = "img/midle_main.png";
            //child.appendChild()
            console.log(elementsGroup  + " length" + elementsData.length);
   
            
            if (first && elementsGroup.length == 4) {
                first = false
                var main_element = elementsGroup[0];
                var ancestorTile = (isCategory) ? contentBuilder.buildCategoryElement(imgBG_big, main_element, document.createElement("div"),false) 
                                    : contentBuilder.buildPromotionElement(imgBG_big, main_element, document.createElement("div"), "",false);
                mainContainer.appendChild(ancestorTile);
                elementsGroup.shift();
                var ancestorChildTile = document.createElement("div");
                for (var i = 0, len = elementsGroup.length; i < len; i++) {
                    if (isCategory) {
                        ancestorChildTile.appendChild(contentBuilder.buildCategoryElement(imgBG_small, elementsGroup[i], null, false));
                    } else {
                        ancestorChildTile.appendChild(contentBuilder.buildPromotionElement(imgBG_small, elementsGroup[i], null, "square", false));
                    }
                }
                ancestorChildTile.className = "tile is-ancestor";
                mainContainer.appendChild(ancestorChildTile);
                
            } else {
                var ancestorChildTile = document.createElement("div");
                var ancestorVertical = document.createElement("div");
                var singleTile = document.createElement("div");
                var parentTile = document.createElement("div");
                var SquareTileParent = document.createElement("div");
                var specialChildParent = document.createElement("div");

                ancestorChildTile.className = "tile is-ancestor";
                ancestorVertical.className = "tile is-vertical is-8";
                singleTile.className = "tile";
                SquareTileParent.className = "tile";
                parentTile.className = "tile is-parent is-vertical tile_card effect__random " + animation[Math.floor(Math.random() * animation.length)];
                specialChildParent.className = "tile special-child tile_card effect__random " + animation[Math.floor(Math.random() * animation.length)];

                if (elementsGroup.length == 4) {
                    for (var i = 0, len = elementsGroup.length; i < len; i++) {
                        var className = "", imageBG = "";
                        var singleParentTile = null;
                        if (i == 3) {
                            className = "square vertical" 
                            imageBG = imgBG_med_v;
                            if (isCategory) {
                                singleParentTile = contentBuilder.buildCategoryElement(imageBG, elementsGroup[i], null, false);
                            } else {
                                singleParentTile = contentBuilder.buildPromotionElement(imageBG, elementsGroup[i], null, className, false);
                            }
                        } else {
                            if (i == 0) {
                                className = "square horizontal"
                                imageBG = imgBG_med_h;
                                if (isCategory) {
                                    parentTile.appendChild(contentBuilder.buildCategoryElement(imageBG, elementsGroup[i], null, true));
                                } else {
                                    parentTile.appendChild(contentBuilder.buildPromotionElement(imageBG, elementsGroup[i], null, className, true));
                                }
                            } else {
                                className = "square";
                                imageBG = imgBG_small;
                                if (isCategory) {
                                    specialChildParent.appendChild(contentBuilder.buildCategoryElement(imageBG.cloneNode(true), elementsGroup[i], null, true));
                                } else {
                                    specialChildParent.appendChild(contentBuilder.buildPromotionElement(imageBG.cloneNode(true), elementsGroup[i], null, className, true));
                                }
                                SquareTileParent.appendChild(specialChildParent.cloneNode(true));                   
                                while (specialChildParent.hasChildNodes()) {
                                    specialChildParent.removeChild(specialChildParent.lastChild);
                                }
                            }
                            
                        }
                    }

                    parentTile.appendChild(SquareTileParent);
                    singleTile.appendChild(parentTile);
                    ancestorVertical.appendChild(singleTile);
                    if (itemFlag){
                        ancestorChildTile.appendChild(ancestorVertical);
                        ancestorChildTile.appendChild(singleParentTile);
                        itemFlag = false;
                    } else {
                        ancestorChildTile.appendChild(singleParentTile);
                        ancestorChildTile.appendChild(ancestorVertical);
                    }
                    mainContainer.appendChild(ancestorChildTile);
                } else {
                    if (elementsGroup.length == 1) {
                        var main_element = elementsGroup[0];
                        var ancestorTile = (isCategory) ? contentBuilder.buildCategoryElement(imgBG_big, main_element, document.createElement("div"),false) 
                                    : contentBuilder.buildPromotionElement(imgBG_big, main_element, document.createElement("div"), "",false);
                        mainContainer.appendChild(ancestorTile);
                    } else {
                        for (var i = 0, len = elementsGroup.length; i < len; i++) {
                            if (isCategory) {
                                ancestorChildTile.appendChild(contentBuilder.buildCategoryElement(imgBG_small, elementsGroup[i], null, false));
                            } else {
                                ancestorChildTile.appendChild(contentBuilder.buildPromotionElement(imgBG_small, elementsGroup[i], null, "square", false));
                            }
                        }
                        mainContainer.appendChild(ancestorChildTile);
                    }
                }
                
                mainContainer.appendChild(ancestorChildTile);
            }

            
        });
        //console.log(categories);
    },
    loadCategories: function(){
        $.ajax({
            url:domain+"/api/v1/categories.json",
            data:{token: $('meta[name="csrf-token"]').attr('content')},
            type:"GET",
            crossDomain: true,
            dataType:"json",   
            contentType:this.contentType,
            beforeSend: function() {
                $(".background-loader").addClass("block-background");
                $(".ui-loader").show();
            },
            complete: function() {  
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
            },
            success:function(data)
            {
                //$.mobile.changePage("index.html", { transition: "slideup", changeHash: false });
               contentBuilder.buildGrid(data, true)
               animations.animInterval();
               contentBuilder.childActionBuilder();
            },
            error:function(jqXHR,textStatus,errorThrown)
            {
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
                console.log(jqXHR.responseJSON.message)  
                sessionState.logout();
            }
        });
    },
    loadPromotions: function(catID) {
        $.ajax({
            url:domain+"/api/v1/categories/"+catID+".json",
            data:{token: $('meta[name="csrf-token"]').attr('content')},
            type:"GET",
            crossDomain: true,
            dataType:"json",   
            contentType:this.contentType,
            beforeSend: function() {
                $(".background-loader").addClass("block-background");
                $(".ui-loader").show();
            },
            complete: function() {  
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
            },
            success:function(data)
            {
                //$.mobile.changePage("index.html", { transition: "slideup", changeHash: false });
               promotionDataArray = data;
               contentBuilder.buildGrid(data, false);
               contentBuilder.childActionPromotionDetailsBuilder();
            },
            error:function(jqXHR,textStatus,errorThrown)
            {
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
                console.log(jqXHR.responseJSON.message)
                //alert(jqXHR.responseJSON.message);
                sessionState.logout();
            }
        });
    },
    countPromotion: function(promID) {
        $.ajax({
            url:domain+"/api/v1/promotions/"+promID+".json",
            data:{token: $('meta[name="csrf-token"]').attr('content')},
            type:"GET",
            crossDomain: true,
            dataType:"json",   
            contentType:this.contentType,
            beforeSend: function() {
                $(".background-loader").addClass("block-background");
                $(".ui-loader").show();
            },
            complete: function() {  
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
            },
            success:function(data)
            {

            },
            error:function(jqXHR,textStatus,errorThrown)
            {
                $(".ui-loader").hide();
                $(".background-loader").removeClass("block-background");
                console.log(jqXHR.responseText);
            }
        });
    },
    childActionBuilder: function(){
        var children = document.querySelectorAll('.mobile-category .is-child');
        [].forEach.call(children, function(child) {
            child.onclick = function() {//window.location.replace("commerce_list.html#"+child.dataset.id)}; 
                contentBuilder.categorySelected = child.dataset.id;
                $.mobile.pageContainer.pagecontainer('change', "commerce_list.html", {
                    transition: 'pop',
                    reload    : true,
                });
            }
        });
    },
    childActionPromotionDetailsBuilder: function(){
        var strong = document.createElement("strong");
        var em = document.createElement("em");
        var children = document.querySelectorAll('.mobile-promotion .is-child');
        [].forEach.call(children, function(child) {
            child.onclick = function() {
                contentBuilder.promotionSelected = child.dataset.id;
                contentBuilder.promotionIdSelected = child.dataset.idPromotion;
                console.log(promotionDataArray[contentBuilder.promotionSelected]);
                var logo = document.getElementById("commerceLogo");
                var promoTitle = document.getElementById("promoTitle");
                var promoDescription = document.getElementById("promoDescription");
                var promoCondition = document.getElementById("promoCondition");
                var promotion = promotionDataArray[contentBuilder.promotionSelected];
                em.innerHTML = promotion.promocion.nombre
                strong.appendChild(em);
                promoTitle.appendChild(strong);
                promoDescription.innerHTML = promotion.promocion.descripcion;
                promoCondition.innerHTML = promotion.promocion.condiciones;
                logo.src = domain + promotion.comercio.logo;0
                $('.bottom-panel').animate({'bottom':'0'},300);
                contentBuilder.countPromotion(contentBuilder.promotionIdSelected);
            }
        });
    },
    promotionDataArray: null,
    promotionSelected: "",
    categorySelected: "",
    promotionIdSelected: ""
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

var authentication = {
    init: function(){
        this.logOut();
        this.logIn();
        this.activate();
        this.changePassword();
        /*this.signUp();*/
        this.resetPassword();
        this.sendMessage();
    },
    contentType: function(){
        var contentType ="application/x-www-form-urlencoded; charset=utf-8";
        if(window.XDomainRequest) {
            contentType = "text/plain";
        } //for IE8,IE9
        return contentType;
    },
    activate: function(){
        $('#activate_user').validate({
            submitHandler: function(form) { 
                var datosUsuario = $("#activate_email").val();
                var datosCarnet = $("#employee_id").val();
                $.ajax({
                    url:domain+"/api/v1/activate_account.json",
                    data:"email="+datosUsuario+"&carnet="+datosCarnet,
                    type:"GET",
                    crossDomain: true,
                    dataType:"json",   
                    contentType:this.contentType,
                    beforeSend: function() {
                        $(".background-loader").addClass("block-background");
                        $(".ui-loader").show();
                    },
                    complete: function() {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                    },
                    success:function(data)
                    {
                        alert(data.message);
                        var home = data.back_home
                        if (home){
                            sessionState.logout();
                        }
                    },
                    error:function(jqXHR,textStatus,errorThrown)
                    {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                        console.log(jqXHR);
                        alert(jqXHR.responseJSON.message);
                    }
                });
            }
        });
    },
    logOut: function(){
        $(".logout").each(function(){
            $(this).click(function(){
                $.ajax({
                    url:domain+"/api/v1/tokens/"+$('meta[name="csrf-token"]').attr('content'),
                    data:"",
                    type:"DELETE",
                    crossDomain: true,
                    dataType:"json",   
                    contentType:this.contentType,
                    beforeSend: function() {
                        $(".background-loader").addClass("block-background");
                        $(".ui-loader").show();
                    },
                    complete: function() {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                    },
                    success:function(data)
                    {
                        //$.mobile.changePage("index.html", { transition: "slideup", changeHash: false });
                        sessionState.logout();
                    },
                    error:function(jqXHR,textStatus,errorThrown)
                    {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                        //console.log(jqXHR.responseJSON.message)
                        //alert(jqXHR.responseJSON.message);
                        sessionState.logout();
                    }
                });
            });
        });
    },
    logIn: function(){
        $('#log_user').validate({
            submitHandler: function(form) { 
                var datosUsuario = $("#user_email").val();
                var datosPassword = $("#user_password").val();
                $.ajax({
                    url:domain+"/api/v1/tokens.json",
                    data:"email="+datosUsuario+"&password="+datosPassword,
                    type:"POST",
                    crossDomain: true,
                    dataType:"json",   
                    contentType:this.contentType,
                    beforeSend: function() {
                        $(".background-loader").addClass("block-background");
                        $(".ui-loader").show();
                    },
                    complete: function() {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                    },
                    success:function(data)
                    {
                        app.setNotification(true);
                        window.plugins.PushbotsPlugin.updateAlias(datosUsuario);
                        var token = data.token
                        //$.mobile.changePage("home.html", { transition: "slideup", changeHash: false });
                        $('meta[name="csrf-token"]').attr('content', token);
                        cookies.deleteCookie("access_token");
                        cookies.setCookie("access_token",token);
                        if($("#remember_me").is(':checked')){               
                              window.localStorage["username_rem"] = datosUsuario;
                        }else{
                          window.localStorage["username_rem"] = "";
                        }
                        if (data.change_password) {
                            window.localStorage["pending_pass"] = true;
                            window.location.replace("activation_change_password.html");
                        } else {
                            window.localStorage["pending_pass"] = false;
                            if(window.localStorage["alert_enable"] == undefined ) {
                                window.localStorage["alert_enable"] = "enabled"; 
                                $(".alert_image").attr('src',"img/footer/alert.png");
                            }
                            window.location.replace("main_menu.html");
                        }
                    },
                    error:function(jqXHR,textStatus,errorThrown)
                    {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                        console.log(jqXHR.responseJSON.message);
                        alert(jqXHR.responseJSON.message);
                        sessionState.logout();
                        if (jqXHR.responseJSON.activate) {
                            window.location.replace("activate_account.html");
                        }
                    }
                });
            }
        });        
    },
    changePassword: function() {
        $('#change_user_password').validate({
            submitHandler: function(form) { 
                var newPass = $("#new_password").val();
                var newPassConfirmation = $("#new_password_confirmation").val();
                $.ajax({
                    url: domain+"/api/v1/change_password/"+$('meta[name="csrf-token"]').attr('content'),
                    data:"password="+newPass+"&password_confirmation="+newPassConfirmation,
                    type:"PUT",
                    crossDomain: true,
                    dataType:"json",   
                    contentType:this.contentType,
                    beforeSend: function() {
                        $(".background-loader").addClass("block-background");
                        $(".ui-loader").show();
                    },
                    complete: function() {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                    },
                    success:function(data) {
                        console.log(data.message)
                        alert(data.message);
                        window.localStorage["pending_pass"] = false;
                        sessionState.logout();
                    },
                    error:function(jqXHR,textStatus,errorThrown) {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                        console.log(jqXHR.responseJSON.message)
                        alert(jqXHR.responseJSON.message);
                    }
                });
            }
        });
    },
    signUp: function(){
        $('#new_user').validate({
            submitHandler: function(form) {
                var datosUsuario = $("#sign_up_user_email").val();
                var datosPassword = $("#sign_up_password").val();
                $.ajax({
                    url:domain+"/api/v1/registrations.json",
                    data:"user[email]="+datosUsuario+"&user[password]="+datosPassword,
                    type:"POST",
                    crossDomain: true,
                    dataType:"json",   
                    contentType:this.contentType,
                    beforeSend: function() {
                        $(".background-loader").addClass("block-background");
                        $(".ui-loader").show();
                    },
                    complete: function() {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                    },  
                    success:function(data) {
                        alert("Usuario Creado Exitosamente: "+JSON.stringify(data.email));
                        var token = data.token;
                        $('meta[name="csrf-token"]').attr('content', token);
                        cookies.deleteCookie("access_token");
                        cookies.setCookie("access_token",token);
                        window.location.replace("home.html");
                    },
                    error:function(jqXHR,textStatus,errorThrown) { 
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                        var message = "";
                        var jsonData = jqXHR.responseJSON
                        for(var obj in jsonData){
                            if(jsonData.hasOwnProperty(obj)){
                                message += obj+': '+jsonData[obj]+"\n";
                            }
                        }
                        alert(message);
                    }
                }); 
            }
        });
    },
    resetPassword: function() {
        $('#send_password_instructions').validate({
            submitHandler: function(form) {
                var datosUsuario = $("#reset_user_email").val();
                console.log(datosUsuario);
                $.ajax({
                    url:domain+"/api/v1/reset_passwords.json",
                    data:"user[email]="+datosUsuario,
                    type:"GET",
                    crossDomain: true,
                    dataType:"json",
                    contentType:this.contentType,
                    beforeSend: function() {
                        $(".background-loader").addClass("block-background");
                        $(".ui-loader").show();
                    },
                    complete: function() {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                    },
                    success:function(data) {
                        alert(data.message);
                        var home = data.back_home
                        if (home){
                            sessionState.logout();
                        }
                    },
                    error:function(jqXHR,textStatus,errorThrown) {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                        console.log(jqXHR.responseJSON.message)
                        alert(jqXHR.responseJSON.message);
                    }
                }); 
            }
        });  
    },
    sendMessage: function() {
        $('#send_message').validate({
            submitHandler: function(form) { 
                var subject = $("#msg_title").val();
                var description = $("#msg_description").val();
                $.ajax({
                    url: domain+"/api/v1/feed_backs",
                    data:{token: $('meta[name="csrf-token"]').attr('content'), 
                        feed_back: { title: subject, description: description}
                    },
                    type:"POST",
                    crossDomain: true,
                    dataType:"json",   
                    contentType:this.contentType,
                    beforeSend: function() {
                        $(".background-loader").addClass("block-background");
                        $(".ui-loader").show();
                    },
                    complete: function() {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                    },
                    success:function(data) {
                        console.log(data.message)
                        $("#msg_title").val("");
                        $("#msg_description").val("");
                        alert(data.message);
                    },
                    error:function(jqXHR,textStatus,errorThrown) {
                        $(".ui-loader").hide();
                        $(".background-loader").removeClass("block-background");
                        console.log(jqXHR.responseJSON.message)
                        alert(jqXHR.responseJSON.message);
                    }
                });
            }
        });  
    },
};