package com.cowbell.cordova.geofence;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by grmorales on 11/28/2016.
 */
public class CancelReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Logger logger = Logger.getLogger();
        int notificationId = intent.getIntExtra("notificationId", 0);
        //logger.log(Log.DEBUG, "NOTIFICATION ID > " + notificationId);
        // Do what you want were.
        //..............
        //..............

        // if you want cancel notification
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(notificationId);
    }
}