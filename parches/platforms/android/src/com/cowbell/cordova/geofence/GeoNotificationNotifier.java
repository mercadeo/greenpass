package com.cowbell.cordova.geofence;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.lafise.greenpass.R;

import java.util.Random;

public class GeoNotificationNotifier {
    private NotificationManager notificationManager;
    private Context context;
    private BeepHelper beepHelper;
    private Logger logger;

    public GeoNotificationNotifier(NotificationManager notificationManager,
            Context context) {
        this.notificationManager = notificationManager;
        this.context = context;
        this.beepHelper = new BeepHelper();
        this.logger = Logger.getLogger();
    }

    public void notify(Notification notification) {
        notification.setContext(context);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setVibrate(notification.getVibrate())
                .setContentText(notification.getText())
                .setSmallIcon(notification.getSmallIcon())
                .setLargeIcon(notification.getLargeIcon())
                .setContentTitle(notification.getTitle())
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notification.getText()));


        if (notification.openAppOnClick) {
            String packageName = context.getPackageName();
            Intent resultIntent = context.getPackageManager()
                    .getLaunchIntentForPackage(packageName);

            if (notification.data != null) {
                //resultIntent.putExtra("geofence.notification.data"+notification.id, notification.getDataJson());
                resultIntent.putExtra("geofence.notification.data", notification.getDataJson());
                resultIntent.putExtra("notificationId", notification.id);
            }
            // The stack builder object will contain an artificial back stack
            // for the
            // started Activity.
            // This ensures that navigating backward from the Activity leads out
            // of
            // your application to the Home screen.

            // Adds the back stack for the Intent (but not the Intent itself)
            // Adds the Intent that starts the Activity to the top of the stack
            resultIntent.setAction(Long.toString(System.currentTimeMillis()));
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                    0, PendingIntent.FLAG_UPDATE_CURRENT);
            //FOR ACTION ON CLICK NOTIFICATION
            mBuilder.setContentIntent(resultPendingIntent);
            //END

            Intent dismissIntent = new Intent(context, CancelReceiver.class);
            dismissIntent.setAction("com.cowbell.cordova.geofence.DISMISS_ACTION");
            dismissIntent.putExtra("notificationId", notification.id);
            PendingIntent piDismiss = PendingIntent.getBroadcast(context, notification.id, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            //context.sendBroadcast(dismissIntent);

            mBuilder.addAction(R.drawable.ic_stat_dismiss, "CANCELAR", piDismiss);//getString(R.string.dismiss), piDismiss)
            mBuilder.addAction(R.drawable.notification_icon, "Ver",resultPendingIntent);
        }
        try {
            Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notificationSound);
            r.play();
        } catch (Exception e) {
            beepHelper.startTone("beep_beep_beep");
            e.printStackTrace();
        }
        notificationManager.notify(notification.id, mBuilder.build());
        logger.log(Log.DEBUG, notification.toString());
    }
}
